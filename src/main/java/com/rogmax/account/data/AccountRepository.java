package com.rogmax.account.data;

import com.rogmax.account.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author vkorbut
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

}
