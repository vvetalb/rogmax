package com.rogmax.account.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author vkorbut
 */
@Entity
@NoArgsConstructor
@Data
public class Account {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Integer age;

}
