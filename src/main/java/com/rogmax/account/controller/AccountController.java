package com.rogmax.account.controller;

import com.rogmax.account.data.AccountRepository;
import com.rogmax.account.model.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

/**
 * @author vkorbut
 */
@Slf4j
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping(path = "/all")
    public List<Account> getAll() {
        return accountRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Object> getAccount(@PathVariable long id) {
        Optional<Account> account = accountRepository.findById(id);

        if (!account.isPresent()) {
            log.warn("Account: {} is not present in database", account);
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(account.get());
    }

    @PostMapping
    public ResponseEntity<Object> createAccount(@RequestBody Account account) {
        Account savedAccount = accountRepository.save(account);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(savedAccount.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<Object> updateAccount(@RequestBody Account account, @PathVariable Long id) {
        Optional<Account> accountOptional = accountRepository.findById(id);

        if (!accountOptional.isPresent()) {
            log.warn("Account {} is not found", account);
            return ResponseEntity.notFound().build();
        }

        account.setId(id);
        accountRepository.save(account);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "{id}")
    public void deleteAccount(@PathVariable Long id) {
        // TODO: catch empty result
        accountRepository.deleteById(id);
    }

}
